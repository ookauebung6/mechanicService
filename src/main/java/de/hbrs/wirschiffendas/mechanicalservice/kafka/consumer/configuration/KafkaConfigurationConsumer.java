package de.hbrs.wirschiffendas.mechanicalservice.kafka.consumer.configuration;

import de.hbrs.wirschiffendas.data.entity.Configuration;
import de.hbrs.wirschiffendas.mechanicalservice.MechanicalServiceApplication;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConfigurationConsumer {

    private MechanicalServiceApplication mechanicalServiceApplication;

    public KafkaConfigurationConsumer (MechanicalServiceApplication mechanicalServiceApplication) {
        this.mechanicalServiceApplication = mechanicalServiceApplication;
    }

    @KafkaListener(topics = "ooka_hauserweber_configuraton_mechanicalervice", groupId = "ookahauserweber.wirschiffendas.mechanic_consumer")
    public void listenConfiguration (Configuration configuration) {
        System.out.println("Kafka: Message eingegangen");
        mechanicalServiceApplication.checkConfig(configuration);
    }
}
