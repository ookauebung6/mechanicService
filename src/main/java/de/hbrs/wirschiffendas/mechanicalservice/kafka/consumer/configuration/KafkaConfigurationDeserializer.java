package de.hbrs.wirschiffendas.mechanicalservice.kafka.consumer.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.Configuration;
import org.apache.kafka.common.serialization.Deserializer;

import java.nio.charset.StandardCharsets;

public class KafkaConfigurationDeserializer implements Deserializer<Configuration> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Configuration deserialize(String s, byte[] bytes) {
        try {
            return objectMapper.readValue(new String(bytes, StandardCharsets.UTF_8), Configuration.class);
        } catch (Exception e) {
            System.err.println("Configuration konnte nicht deserialisiert werden.");
            e.printStackTrace();
            return null;
        }
    }
}
