package de.hbrs.wirschiffendas.mechanicalservice.kafka.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.TransferItems.StatusTransferItem;
import org.apache.kafka.common.serialization.Serializer;

import java.util.Map;

public class KafkaStatusSerializer implements Serializer<StatusTransferItem> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {
    }

    @Override
    public byte[] serialize(String s, StatusTransferItem statusTransferItem) {
        try {
            return objectMapper.writeValueAsBytes(statusTransferItem);
        } catch (JsonProcessingException e) {
            System.err.println("Objekt konnte nicht serialisiert werden!");
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void close() {
    }
}
