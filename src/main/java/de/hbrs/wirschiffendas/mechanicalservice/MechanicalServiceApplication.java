package de.hbrs.wirschiffendas.mechanicalservice;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.hbrs.wirschiffendas.data.entity.*;
import de.hbrs.wirschiffendas.data.entity.TransferItems.ResultTransferItem;
import de.hbrs.wirschiffendas.data.entity.TransferItems.StatusTransferItem;
import de.hbrs.wirschiffendas.mechanicalservice.kafka.producer.KafkaStatusProducer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

import static de.hbrs.wirschiffendas.data.entity.AlgorithmIdentifier.MECHANICAL;

@SpringBootApplication
@RestController
@EnableAsync
@Component
public class MechanicalServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MechanicalServiceApplication.class, args);
    }


    //-----------
    //   KAFKA
    //-----------

    public void checkConfig (Configuration configuration) {
        performWait();
    }


    //-----------
    // END KAFKA
    //-----------

    void sendRunning() {
        Status status = Status.RUNNING;
        try {
            new KafkaStatusProducer().sendStatus(status);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Async
    void sendSuccess() {
        Status status = Status.READY;
        try {
            new KafkaStatusProducer().sendStatus(status);
        } catch (Exception e) {
            e.printStackTrace();
        }

        ResultTransferItem resultTransferItem = new ResultTransferItem(Result.SUCCESS, MECHANICAL);
        String resultUrl = "http://10.26.0.6:8080/resultUpdate";

        RestTemplate resultRestTemplate = new RestTemplate();
        try {
            URI uri = new URI(resultUrl);
            ResponseEntity<ResultTransferItem> responseEntity = resultRestTemplate.postForEntity(uri, resultTransferItem, ResultTransferItem.class);
            System.out.println(responseEntity.getStatusCode());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    private void performWait() {
        sendRunning();
        System.out.println("before sleep");

        try {
            Thread.sleep(10 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("after sleep");

        sendSuccess();

        System.out.println("after send");
    }
}

